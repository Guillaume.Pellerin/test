# Test audio in MD on GitLab

[Transformed](https://forum.ircam.fr/media/uploads/software/ANGUS/f1a.pitch1_.orig_transp.ratio-3_0.mix-0_75.rough_.rmsnorm.wav)
<p>
    <audio controls>
        <source src="https://forum.ircam.fr/media/uploads/software/ANGUS/f1a.pitch1_.orig_transp.ratio-3_0.mix-0_75.rough_.rmsnorm.wav">
    </audio>
</p>

![https://forum.ircam.fr/media/uploads/software/ANGUS/f1a.pitch1_.orig_transp.ratio-3_0.mix-0_75.rough_.rmsnorm.wav](https://forum.ircam.fr/media/uploads/software/ANGUS/f1a.pitch1_.orig_transp.ratio-3_0.mix-0_75.rough_.rmsnorm.wav)